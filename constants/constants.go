/*
	go build/run -ldflags "-X bitbucket.org/mike-dev/api-derayah-standard-endpoints/info/infrastructure.AppName=defined_name" main.go
*/
package constants

// GitBranchName represents git branch name
// example: master
var GitBranchName string

// GitCommitID represents git commit id
// example: 0a2d11b
var GitCommitID string

// GitCommitTime represents git commit creation time
// example: 2020-08-31 16:59:20
var GitCommitTime string

// GitTags represents git tags; if multiple tags provided they should be {GitTagsSep}-separated
// example: 4.5.2+2435,some-other-tag,v1.2.3
var GitTags string

// GitTagsSep is a separator used to split git tags
const GitTagsSep = ","

// GitTagsSep go.mod  contains proto deps
// example: store v0.1.11
var ProtoDeps = map[string]string{}
