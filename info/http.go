package info

import (
	"encoding/json"
	"net/http"
	"strings"

	"bitbucket.org/mike-dev/api-derayah-standard-endpoints/constants"
	"bitbucket.org/mike-dev/api-derayah-standard-endpoints/errors"
	"github.com/go-kit/kit/log"
	"github.com/gorilla/mux"
)

// info represents all of the service-related info
type info struct {
	Git git `json:"git"`
}

type git struct {
	BranchName string `json:"branch"`
	Commit     commit `json:"commit"`
}
type commit struct {
	ID   string   `json:"id"`
	Tags []string `json:"tags"`
	Time string   `json:"time"`
}

const infoURL = "/info"

// AddInfoEndpoint provides version endpoint which is used to determine service info used (app name/app version/branch name/commit id/commit tags)
func AddInfoEndpoint(rtr *mux.Router, logger log.Logger, branchName, commitID, commitTags, commitTime string) {
	rtr.Methods(http.MethodGet).Path(infoURL).HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set("Content-Type", "application/json")
		responseStatus := http.StatusOK

		commitTags = strings.Replace(commitTags, "\n", "", -1)
		tags := strings.Split(commitTags, constants.GitTagsSep)
		lt := len(tags)
		if lt > 1 && tags[lt-1] == "" {
			tags = tags[0 : lt-1]
		}
		message := info{
			Git: git{
				BranchName: branchName,
				Commit: commit{
					ID:   commitID,
					Tags: tags,
					Time: commitTime,
				},
			},
		}

		jsonReturn, err := json.Marshal(message)
		if err != nil {
			errors.Handle(logger, err, "failed marshalling message response")
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}

		rw.WriteHeader(responseStatus)

		_, err = rw.Write(jsonReturn)
		if err != nil {
			errors.Handle(logger, err, "failed writing the data to HTTP response")
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}
	})
}
