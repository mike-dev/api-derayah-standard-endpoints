package info

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-kit/kit/log"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
)

var commitTags = []string{
	"v0.1.0",
	"v0.2.0",
}

const (
	appName         = "test_service"
	appVersion      = "v0.0.1"
	appCreationTime = "2020-08-31 16:59:20"
	branchName      = "test_branch"
	commitID        = "99ebcd29"
	commitTime      = "2020-08-31 16:59:20"
	cmTags          = `v0.1.0,v0.2.0,
`
)

func TestInfoEndpoint(t *testing.T) {
	expectedMsg := info{
		App: app{
			Name:    appName,
			Version: appVersion,
			Time:    appCreationTime,
		},
		Git: git{
			BranchName: branchName,
			Commit: commit{
				ID:   commitID,
				Tags: commitTags,
				Time: commitTime,
			},
		},
	}

	expectedBody, err := json.Marshal(expectedMsg)
	if err != nil {
		t.Errorf("failed marshalling expected response: %s", err)
	}

	rtr := mux.NewRouter()
	logger := log.NewNopLogger()
	AddInfoEndpoint(
		rtr,
		logger,
		appName,
		appVersion,
		appCreationTime,
		branchName,
		commitID,
		cmTags,
		commitTime,
	)

	req, _ := http.NewRequest(http.MethodGet, infoURL, nil)
	resp := httptest.NewRecorder()
	rtr.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusOK, resp.Code, "HTTP codes are not equal")
	assert.Equal(t, "application/json", resp.Header().Get("Content-Type"), "incorrect Content-Type")
	assert.JSONEq(t, string(expectedBody), resp.Body.String(), "response body is invalid")
}
