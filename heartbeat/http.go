package heartbeat

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/mike-dev/api-derayah-standard-endpoints/errors"
	"github.com/go-kit/kit/log"
	"github.com/gorilla/mux"
)

const heartbeatURL = "/heartbeat"

// heartbeat represents heartbeat properties
type heartbeat struct {
	Status bool `json:"status"`
}

// AddHeartbeatEndpoint provides heartbeat endpoint which is used to determine service status (available/unavailable)
func AddHeartbeatEndpoint(rtr *mux.Router, logger log.Logger) {
	rtr.Methods(http.MethodGet).Path(heartbeatURL).HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set("Content-Type", "application/json")
		responseStatus := http.StatusOK

		var message = heartbeat{
			Status: true,
		}
		jsonReturn, err := json.Marshal(message)
		if err != nil {
			errors.Handle(logger, err, "failed marshalling message response")
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}

		rw.WriteHeader(responseStatus)

		_, err = rw.Write(jsonReturn)
		if err != nil {
			errors.Handle(logger, err, "failed writing the data to HTTP response")
			rw.WriteHeader(http.StatusInternalServerError)
			return
		}
	})
}
