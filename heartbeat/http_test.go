package heartbeat

import (
	"github.com/go-kit/kit/log"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHeartbeatEndpoint(t *testing.T) {
	rtr := mux.NewRouter()
	logger := log.NewNopLogger()
	AddHeartbeatEndpoint(rtr, logger)

	req, _ := http.NewRequest(http.MethodGet, heartbeatURL, nil)
	resp := httptest.NewRecorder()
	rtr.ServeHTTP(resp, req)

	assert.Equal(t, http.StatusOK, resp.Code, "HTTP codes are not equal")
	assert.Equal(t, "application/json", resp.Header().Get("Content-Type"), "incorrect Content-Type")
	assert.JSONEq(t, `{"status": true}`, resp.Body.String(), "response body is invalid")
}
