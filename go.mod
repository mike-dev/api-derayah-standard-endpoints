module bitbucket.org/mike-dev/api-derayah-standard-endpoints

go 1.14

require (
	github.com/go-kit/kit v0.10.0
	github.com/gorilla/mux v1.8.0
	github.com/stretchr/testify v1.4.0
)
