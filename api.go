package api

import (
	"bitbucket.org/mike-dev/api-derayah-standard-endpoints/constants"
	"bitbucket.org/mike-dev/api-derayah-standard-endpoints/heartbeat"
	"bitbucket.org/mike-dev/api-derayah-standard-endpoints/info"
	"github.com/go-kit/kit/log"
	"github.com/gorilla/mux"
)

type standardEndpoints struct {
	rtr    *mux.Router
	logger log.Logger
}

// InitStandardEndpoints initializes standard endpoints based on provided options
func InitStandardEndpoints(rtr *mux.Router, logger log.Logger, options ...Option) {
	se := &standardEndpoints{
		rtr:    rtr,
		logger: logger,
	}
	for _, option := range options {
		option(se)
	}
}

// Option sets an optional parameter for standard endpoints.
type Option func(*standardEndpoints)

// HeartbeatOption set up heartbeat endpoint
// Deprecated: Heartbeat endpoint exists for historical backward compatibility.
// Health endpoint has an extended information about service dependencies status and should be used instead of Heartbeat endpoint
func HeartbeatOption() Option {
	return func(s *standardEndpoints) {
		heartbeat.AddHeartbeatEndpoint(s.rtr, s.logger)
	}
}

// InfoOption set up info endpoint
func InfoOption() Option {
	return func(s *standardEndpoints) {
		info.AddInfoEndpoint(s.rtr, s.logger,
			constants.GitBranchName, constants.GitCommitID, constants.GitTags, constants.GitCommitTime,
		)
	}
}
