# API Standard Endpoints #

### What is this repository for? ###

This repository collects all the standard endpoints implementation which could be used across all the go services. 
Current endpoints list:

- /heartbeat
- /info

```
ENV GOPRIVATE="bitbucket.org/mike-dev"

RUN export GIT_BRANCH_NAME=$(git log -n1 --decorate=short --pretty="tformat:%fd") && \
   GIT_COMMIT_ID=$(git rev-parse --short HEAD) && \
   GIT_COMMIT_TIME=$(git --no-pager log -1 --date=format:"%Y-%m-%d|%T" --format="%ad") && \
   GIT_TAGS=$( git tag --format="%(refname:short)," | sort -V | tr -d '\n' | sed 's/%//') && \
   go build -ldflags \
    '-X "'$GOPRIVATE'/api-derayah-standard-endpoints/constants.GitBranchName='$GIT_BRANCH_NAME'" \
     -X "'$GOPRIVATE'/api-derayah-standard-endpoints/constants.GitCommitID='$GIT_COMMIT_ID'" \
     -X "'$GOPRIVATE'/api-derayah-standard-endpoints/constants.GitCommitTime='$GIT_COMMIT_TIME'" \
     -X "'$GOPRIVATE'/api-derayah-standard-endpoints/constants.GitTags='$GIT_TAGS'"  main.go

```

