package errors

import (
	"errors"
	"github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

// Handle processes error
func Handle(logger log.Logger, err error, msg string) {
	err = level.Error(logger).Log("message", msg, "error", err)
	// in case if logger is not correctly set up panic should occur
	if err != nil {
		panic(err)
	}
}

// New returns a new error based on provided message
func New(msg string) error {
	return errors.New(msg)
}
